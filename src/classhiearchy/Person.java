package classhiearchy;

public class Person {
	private String name;
	private String surname;
	private int age;
	private String job;
	private String country;

	/*Constructor*/
	public Person(String name) {
		this.name = name;
	}

	/*Method to display name*/
	public void displayName() {
		System.out.println(name);
	}

}
