package classhiearchy;

public class Teacher extends Person {
	private String tittle;

	/* Constructor */
	public Teacher(String name, String tittle) {
		super(name);
		this.tittle = tittle;
	}
	
	/*Method to display name and tittle*/
	public void displayName() {
		System.out.print(tittle + " ");
		super.displayName();
	}

}
